// import dependencies you will use
const express = require('express');
const path = require('path');
//setting up Express Validator
const {check, validationResult} = require('express-validator'); // ES6 standard for destructuring an object

// set up variables to use packages
var myApp = express();
myApp.use(express.urlencoded({extended:false}));

// set path to public folders and view folders

myApp.set('views', path.join(__dirname, 'views'));
//use public folder for CSS etc.
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine', 'ejs');
// set up different routes (pages) of the website

//home page
myApp.get('/', function(req, res){
    res.render('form'); // no need to add .ejs to the file name
});

//form submission handler
myApp.post('/', [
    check('name', 'Must have a name').notEmpty(),
    check('email', 'Must have email').isEmail()
    
],function(req, res){

    const errors = validationResult(req);
    if (!errors.isEmpty()){
        //console.log(errors); // check what is the structure of errors
        res.render('form', {
            errors:errors.array()
        });
    }
    else{
        var name = req.body.name;
        var email = req.body.email;
        var phone = req.body.phone;
        var postcode = req.body.postcode;
        var tickets = req.body.tickets;
        var lunch = req.body.lunch;
        var campus = req.body.campus;

        var subTotal = tickets * 20;
        if(lunch == 'yes'){
            subTotal += 15;
        }
        var tax = subTotal * 0.13;
        var total = subTotal + tax;

        var pageData = {
            name : name,
            email : email,
            phone : phone, 
            postcode : postcode,
            lunch : lunch,
            tickets : tickets,
            campus : campus,
            subTotal : subTotal,
            tax : tax,
            total : total
        }
        res.render('form', pageData);
    }
});


//author page
myApp.get('/author',function(req,res){
    res.render('author',{
        name : 'Davneet Chawla',
        studentNumber : '123123'
    }); 
});

// start the server and listen at a port
myApp.listen(8080);

//tell everything was ok
console.log('Everything executed fine.. website at port 8080....');


